import numpy as np
from scipy.optimize import bisect 

### ### ### FUNCTIONALS | Functions in this file assign a numerical value to a function
### ### ### ############# much like integration or any other fuctional

### Function | Get maximum value of pressure between time 0 and a specified value
def get_pmax(fun, tfin = 2, steps = 1000):

    spc = np.linspace(0, tfin, steps)
    y = fun(spc)
    return np.max(y)


### Function | Get rising times for presures between 5% and 90% AND between 10% and 90%
def get_trs(fun, tfin = 2, P_amb = 1e5, calibration = False):

    vals = np.arange(0, tfin, 0.01)

    tmax = 0
    highest = 0

    for val in vals:
        test = fun(val)
        if test > highest:
            highest = test
            tmax = val

    if calibration:
        pmax = fun(tfin)
    else:
        pmax = fun(tmax)

    p05 = (pmax - P_amb) * 0.05 + P_amb    
    p10 = (pmax - P_amb) * 0.10 + P_amb
    p90 = (pmax - P_amb) * 0.90 + P_amb

    f05 = lambda t: fun(t) - p05
    f10 = lambda t: fun(t) - p10
    f90 = lambda t: fun(t) - p90

    t05 = bisect(f05, 0.0, tmax)
    t10 = bisect(f10, t05, tmax)
    t90 = bisect(f90, t10, tmax)

    if t05 == 0: t05 = 0.05 * tfin
    if t10 == 0: t10 = 0.10 * tfin
    if t90 == 0: t90 = 0.90 * tfin


    return t90 - t05, t90 - t10


### Function | Get maximum climb rate of specified function between 0 and specified argument
def get_ratemax(fun, tfin = 2, steps = 200):

    spc = np.linspace(0, tfin, steps)
    y = fun(spc)
    dermax = 0

    for i in range(1, steps):

        der = y[i] - y[i-1]
        if der > dermax:
            dermax = der

    return dermax * steps / tfin

### Function | Get overshoot 
def get_overshoot(fun, tfin = 2):

    pmax = get_pmax(fun, tfin=tfin)

    return (pmax - fun(tfin)) / pmax
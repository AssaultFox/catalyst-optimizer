import numpy as np
import cantera as ct
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp as ivp
from scipy.interpolate import interp1d as i1d

### ### ### SIMULATE | Simulates a behaviour of a test chamber connected to a catalytic bed with fixed propellant injection rate
### ### ### ########## This model should be interpreted only with its theoretical basis, as it is based on assumptions
### ### ### ########## which can only satisfy certain type of investigation.
### ### ### ########## Certain amount of suboptimal global variable acrobatics had to be done to satisfy the way cantera simulations work

### Default values to be overwritten with each call of simulate
mdot_0 = 0          # Mass flow rate of propellant [kg/s]
time_feed = 0       # Flow time [s]
sim_time = 1.5      # Total simulation time [s]

E0_g = 5e4          # Acticvtion energy for catalyst
bed_heatcap = 16    # Total heat capacity of the catalyst bed [J/K]

ml0 = 0             # Initial mass of propellant in the chamber [kg]
P_amb = 1e5         # Ambient atmospheric pressure [Pa]
T_amb = 293         # Ambient temperature [K]

### Constants
R = 8315            # Universal gas constant
Cd = 1.3514         # Nozzle discharge coefficient [-]

### ### EXECUTION ##############################################################

### Global variables to pass parameters to functions
k_g = 0
At_g = 0
T_g = None

### Calculate total enthalpy of perfectly decomposed HTP from teoretical decomposition products
gas = ct.Solution('gri30.yaml')
gas.TPX = 1218, P_amb, {'O2':0.325, 'H2O': 0.675}
H1220 = gas.HP[0]

### Function | calculate gas enthalpy based on temperature
def enthalpy(T):
    
    gas.TPX = T, 1e5, gas.X
    
    return gas.HP[0]

### Function | calculate propellant flow through injector based on time
def mdot_in(t):
    
    if t <= time_feed:
        return mdot_0
    
    else:
        return 0
    
### Function | calculate reaction rate [kg/s] in catalyst bed
def mdot_gas(ml, Tz):

    return ml * k_g * np.exp(-E0_g/(R*Tz))

### Function | calculate state derivative over time, based on time and state
def derivative_vector(t, state):
    
    X = [0, 0]
    
    mdi = mdot_in(t)
    mdg = mdot_gas(state[0], state[1])
    
    ### Assume perfect thermal equilibrium between outflowing gas and catalyst
    T_out = state[1]
    
    ### Mass of propellant in the catalyst zone increases with injection and decreases with decomposition
    X[0] = mdi - mdg
    ### Temperature in the catalyst zone grows based on how much decomposition enthalpy is left in the bed
    X[1] = mdg * (H1220 - enthalpy(T_out)) / bed_heatcap
    
    return X


### Function | Calculate how fast gas is leaving the chamber
def mdot_out(delta_P):

    gas.TP = T_g.T, delta_P + P_amb

    ### No flow if for some reason ambient pressure exceeds chamber pressure
    if P_amb > gas.P:
        return 0
    
    H0 = gas.HP[0]
    S0 = gas.SP[0]
    
    ### Let's use the dummy gas instance to calculate critical pressure and velocity
    P_crit = gas.P * 0.54
    
    if P_amb > P_crit:
        gas.SP = S0, P_amb
        
    else:
        gas.SP = S0, P_crit
    
    H1 = gas.HP[0]
    
    ### Catch problems if for some reason decompressed gas wants to slow down
    if H1 > H0:
        return 0

    ### From conservation of enthalpy, get flow velocity
    vel = np.sqrt(2 * (H0 - H1))
    rho = gas.density
    
    ### Get outward mass flow
    mdot = vel * rho * At_g

    return mdot 

### Function | Get simulation results in form of pressure profile over time
def simulate(k, e0_T, D, volume, settings_dict, dt = 1e-3):

    ### We are going to be passing some parameters globally, which is suboptimal but functional
    global T_g, k_g, At_g, E0_g
    global mdot_0, time_feed, sim_time, bed_heatcap, P_amb, T_amb, ml0, Tz0

    ### Set value of global variable k_g to match k, passing k to mdot_gas
    k_g = k

    ### Calculate effective throat area
    At_g = Cd * D**2 * np.pi / 4 

    ### Set global activation energy
    E0_g = R * e0_T

    ### Set  other globalvariables using settings dictionary
    mdot_0 = settings_dict['mdot_0']
    time_feed = settings_dict['time_feed']
    sim_time = settings_dict['sim_time']
    bed_heatcap = settings_dict['bed_heatcap']
    P_amb = settings_dict['P_amb']
    T_amb = settings_dict['T_amb']
    ml0 = settings_dict['ml0'] 
    Tz0 = settings_dict['Tz0'] 

    ### Starting chamber condition - accumulated propellant mass and temperature
    VEC = [ml0, Tz0]

    ### Integrate the state equation for decomposition separately, since it's an irreversible reaction anyway
    result = ivp(derivative_vector, (0, sim_time + 1e-2), VEC, method = 'LSODA')
    time1 = result.t
    ml = result.y[0]
    Tz = result.y[1]

    ### Recreate gas production rate from recorded state history
    gas_prod = mdot_gas(ml, Tz)

    ### Turn discrete integration results into an interpolated functions
    gas_temp = i1d(time1, Tz, kind = 'quadratic')
    prod = i1d(time1, gas_prod, kind = 'quadratic')

    G0 = ct.Solution("gri30.yaml")
    G0.TPX = T_amb, P_amb, {'O2':0.325, 'H2O': 0.675}
    
    ### Intitalize cantera reactor network to handle chamber integration
    Res_in = ct.Reservoir(G0)
    Res_out = ct.Reservoir(G0)
    Reac = ct.Reactor(volume = volume, contents = G0)
    T_g = Reac.thermo
    Injector = ct.MassFlowController(Res_in, Reac, mdot = prod)
    Nozzle = ct.Valve(Reac, Res_out, K = mdot_out)
    N = ct.ReactorNet([Reac])

    P_list = [G0.P]
    time2 = [0]
    t = 0

    while N.time < sim_time:
        
        ### Advance the cantera simulation
        t = t + dt
        N.advance(t)

        ### Refresh incoming gas temperature
        G0.TPX = gas_temp(t), 1e5, G0.X
        Res_in.insert(G0)

        ### Refresh the global reactor thermo phase
        T_g = Reac.thermo

        ### Record caluclated parameters
        P_list.append(Reac.thermo.P)
        time2.append(t)

    p_fun = i1d(time2, P_list, kind = 'quadratic', fill_value = 'extrapolate')

    return p_fun

dict = {
        'mdot_0' : 15e-3,
        'time_feed' : 0.3,
        'sim_time' : 1.5,
        'bed_heatcap' : 16,
        'P_amb' : 1e5,
        'T_amb' : 293,
        'ml0' : 0,
        'Tz0' : 293
        }
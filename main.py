from experimental_data import exp_press
from simulate import simulate as sim
from functionals import *
from scipy.optimize import direct as dr
import plotly.graph_objects as go
import matplotlib.pyplot as plt

### ### ### MAIN | This script accomplishes 2 tasks:
### ### ### ###### 1. Finding values of catalyst constants (E0 and k) corresponding to experimental data
### ### ### ###### by recreating an experiment with predefined setup geometry
### ### ### ###### 2. Simulating a set of predefined testing systems and finding among them those
### ### ### ###### most responsive to changes in k, in terms of pressure functionals

### ### EXPERIMENT RECREATION - SETUP

k_start = 10
k_end = 50 
k_steps = 50

e0_T_start = 400
e0_T_end = 1000
e0_T_steps = 50

### Define setup parameters for the experiment
exp_dict = {
        'mdot_0' : 15e-3,
        'time_feed' : 5,
        'sim_time' : 2,
        'bed_heatcap' : 32,
        'P_amb' : 1e5,
        'T_amb' : 293,
        'ml0' : 0,
        'Tz0' : 293
        }

### ### TEST SETUP SIMULATION - SETUP

V_min = 200
V_max = 1000
V_steps = 17

D_min = 0
D_max = 4
D_steps = 17

test_dict = {
        'mdot_0' : 10e-3,
        'time_feed' : 0.3,
        'sim_time' : 2,
        'bed_heatcap' : 16,
        'P_amb' : 1e5,
        'T_amb' : 293,
        'ml0' : 0,
        'Tz0' : 293
        }

### ### EXPERIMENT RECREATION - EXECUTION
### Get reference functionals of the experimental pressure data
exp_t0590, exp_t1090 = get_trs(exp_press, tfin = 2, P_amb = 1e5)
exp_ratemax = get_ratemax(exp_press, tfin = 2, steps = 200)
exp_pmax = get_pmax(exp_press, tfin = 2)

### Define range of investigated catalyst properties
k_space = np.linspace(k_start, k_end, k_steps)
e0_T_space = np.linspace(e0_T_start, e0_T_end, e0_T_steps)

### Function | compare acquired functionals with experimental functionals based on specified k and T
def fun(X):

    k, T = X
    f = sim(k, T, 4e-3, 40e-6, exp_dict, dt=0.01)
    
    t0590, t1090 = get_trs(f, calibration=True)
    ratemax = get_ratemax(f)

    A = ((t0590 - exp_t0590) / exp_t0590)**2
    B = ((t1090 - exp_t1090) / exp_t1090)**2
    C = ((ratemax - exp_ratemax) / exp_ratemax)**2
    D = get_overshoot(f)**2
    
    ### Arbitrarily set weights of functionals in the goal functions 
    val = 0*A + B + 2*C + 4*D
    return val

print("### FINDING EXPERIMENTAL CATALYST PROPERTIES ###")

k_0, T_0 = dr(fun, [(k_start, k_end), (e0_T_start, e0_T_end)], maxfun = 25).x

print("k: ", k_0)
print("T: ", T_0)

### Plot the results to compare pressure profiles in experiment and in simulation

x = np.linspace(0, exp_dict['sim_time'], 1000)
plt.plot(x, sim(k_0, T_0, 4e-3, 40e-6, exp_dict, dt=0.01)(x))
plt.plot(x, exp_press(x))
plt.show()

### Define a function that measures how sensitive different testing setups are to variation in catalyst activity k
### Function | Find sensitivity indicators for given throat area, chamber volume, catalyst activity and activity differential
def get_deltas(D, V, k0, T0, delta):

    sim_time = test_dict['sim_time']

    k1 = k0 * (1 - (delta / 2))
    k2 = k0 * (1 + (delta / 2))

    f1 = sim(k1, T0, D, V, test_dict, dt=0.001)
    f2 = sim(k2, T0, D, V, test_dict, dt=0.001)


    pmax1 = get_pmax(f1, tfin = sim_time)
    pmax2 = get_pmax(f2, tfin = sim_time)

    tr05901, tr10901 = get_trs(f1, tfin = sim_time)
    tr05902, tr10902 = get_trs(f2, tfin = sim_time)

    ratemax1 = get_ratemax(f1, tfin = sim_time)
    ratemax2 = get_ratemax(f2, tfin = sim_time)

    delta_tr0590 =  (2. / delta) * (tr05902 - tr05901) / (tr05901 + tr05902)
    delta_tr1090 =  (2. / delta) * (tr10902 - tr10901) / (tr10901 + tr10902)

    delta_pmax =    (2. / delta) * (pmax2 - pmax1) / (pmax1 + pmax2)
    delta_ratemax = (2. / delta) * (ratemax2 - ratemax1) / (ratemax1 + ratemax2)

    return delta_tr0590, delta_tr1090, delta_pmax, delta_ratemax

### From the previously defined space of throat diameter and chamber volume, start calculating values of deltas

x = np.linspace(V_min, V_max, V_steps)
y = np.linspace(D_min, D_max, D_steps)

z1 = []
z2 = []
z3 = []
z4 = []

for DD in y:

    zz1 = []
    zz2 = []
    zz3 = []
    zz4 = []

    print('DIAMETER: %.3f [mm]\n05-90 [-]\t10-90 [-]\tpmax [-]\tratemax [-]\tV [ccm]\t\tD [mm]' % DD) 
    for VV in x:

        deltas = get_deltas(DD * 1e-3, VV * 1e-6, k_0, T_0, 0.1)

        zz1.append(deltas[0])
        zz2.append(deltas[1])
        zz3.append(deltas[2])
        zz4.append(deltas[3])

        print('%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.1f\t\t%.1f' % (deltas[0], deltas[1], deltas[2], deltas[3], VV, DD))

    z1.append(zz1)
    z2.append(zz2)
    z3.append(zz3)
    z4.append(zz4)


### Plot values of deltas

fig = go.Figure(data = go.Contour(z=z1, x=x, y=y))
fig.update_layout(xaxis_title='Chamber Volume [ccm]', yaxis_title='Throat Diameter [mm]', title='Tr (05-09) DELTA')
fig.show()

fig = go.Figure(data = go.Contour(z=z2, x=x, y=y))
fig.update_layout(xaxis_title='Chamber Volume [ccm]', yaxis_title='Throat Diameter [mm]', title='Tr (10-09) DELTA')
fig.show()

fig = go.Figure(data = go.Contour(z=z3, x=x, y=y))
fig.update_layout(xaxis_title='Chamber Volume [ccm]', yaxis_title='Throat Diameter [mm]', title='P max DELTA')
fig.show()

fig = go.Figure(data = go.Contour(z=z4, x=x, y=y))
fig.update_layout(xaxis_title='Chamber Volume [ccm]', yaxis_title='Throat Diameter [mm]', title='dP/dt max DELTA')
fig.show()